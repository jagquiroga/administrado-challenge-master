<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class UpdateOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza las ordenes desde una Api cada 10 min';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //realiza la consulta a la api
        $response= Http::withHeaders([
            'Authorization' => 'Bearer 12|vU51qlCmnthiayvexDiAYzJvYZW8MyDOFixLDWnM'
        ])->get('http://challenge1.administrado.net/api/orders');
        //guarda la respuesta en un array
        $orders = $response->json();

        //recorre el array y guarda los datos en la base de datos
        foreach ($orders as $order) {
            $order = Order::updateOrCreate(
                ['id' => $order['id']],
                [
                    'id' => $order['id'],
                    'name' => $order['name'],
                    'email' => $order['email'],
                    'total' => $order['total'],
                    'status' => $order['status'],
                    'created_at' => $order['created_at'],
                    'updated_at' => $order['updated_at'],
                ]
            );
        }
        return 0;
    }
}
