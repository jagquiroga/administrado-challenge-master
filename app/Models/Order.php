<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'buyer_name',
        'description',
        'photo_url',
        'address',
        'longitude',
        'latitude',
        'soft_deleted',
        'created_at',
        'updated_at',
    ];
}
